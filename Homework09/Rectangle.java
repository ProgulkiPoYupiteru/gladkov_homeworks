package com.cyberworld;

public class Rectangle extends Figure{

    public Rectangle(int x, int y) {
        super(x, y);
    }

    public int getPerimeter() {
        return x * 2 + y * 2;
    }
}

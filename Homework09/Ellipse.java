package com.cyberworld;

public class Ellipse extends Figure{

    public Ellipse(int x, int y) {
        super(x, y);
    }
    public int getPerimeter() {
        return x * 2 + y * 2;
    }

}

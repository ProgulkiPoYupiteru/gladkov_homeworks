package com.cyberworld;

public class Main {

    public static void main(String[] args) {

        Figure figure = new Figure(3,8);
        Ellipse ellipse = new Ellipse(16, 10);
        Rectangle rectangle = new Rectangle(3, 4);
        Square square = new Square(5);
        Circle circle = new Circle(13);

        System.out.println(figure.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());

    }

}

package com.cyberworld;

public class Circle extends Ellipse implements Move {

    public Circle(int x, int y) {

        super(x, y);
    }

    @Override
    public int toMove() {
        return 0;
    }
}

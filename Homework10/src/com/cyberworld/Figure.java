package com.cyberworld;

public abstract class Figure {

    protected int x;
    protected int y;

    public int getX() {
        return x;
    }


    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int Go(int go) {
        return 0;

    }


    public int toMove(int i) {
        return 0;
    }
}

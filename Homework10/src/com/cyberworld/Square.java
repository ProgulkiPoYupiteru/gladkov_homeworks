package com.cyberworld;

public class Square extends Rectangle implements Move {

    public Square(int x, int y) {

        super(x, y);
    }

    @Override
    public int toMove() {
        return 0;
    }
}

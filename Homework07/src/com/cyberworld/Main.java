package com.cyberworld;


import java.util.Arrays;

public class Main {

    public static void main(String[] args) {


        int[] array = new int[] {-68, -26, 30, 22, 11, -68, -26, 98, 13, 30, 11, 22};
        int i = 0;

        Arrays.sort(array);

        while (i < array.length)
        {
            if (i == array.length-1) {
                System.out.println(array[i++]);
                break;
            }
            if (array[i] == array[i+1])
                i += 2;
            else System.out.println(array[i++]);
        }
    }

}

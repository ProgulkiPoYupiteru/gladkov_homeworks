package com.cyberworld;


import java.util.Arrays;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        Human[] human = new Human[10];

        human[0] = new Human("Alexander", 85);
        human[1] = new Human("Johnny", 70);
        human[2] = new Human("Jane", 50);
        human[3] = new Human("Lucile", 61);
        human[4] = new Human("George", 93);
        human[5] = new Human("Jasmine", 59);
        human[6] = new Human("CJ", 92);
        human[7] = new Human("Joyce", 67);
        human[8] = new Human("Richard", 77);
        human[9] = new Human("Junior", 15);

        Arrays.sort(human, new Comparator<Human>() {
            @Override
            public int compare(Human o1, Human o2) {
                return o1.getWeight() - o2.getWeight();
            }
        });
        for (int i = 0; i < human.length; i++) {
            System.out.println(human[i].getName());
        }



    }
}

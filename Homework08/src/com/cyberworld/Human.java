package com.cyberworld;

public class Human {

    private String name;
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        if (weight < 0 || weight > 120) {
            weight = 0;
        }
        this.weight = weight;
    }

    public Human(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }


}
